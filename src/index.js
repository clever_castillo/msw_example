import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";

function renderApp() {
  ReactDOM.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
    document.getElementById("root")
  );

  reportWebVitals();
}

if (
  process.env.NODE_ENV === "development" &&
  process.env.REACT_APP_MSW === "true"
) {
  const { worker } = require("./mocks/browser");
  worker.start().then(() => {
    renderApp();
  });
} else {
  renderApp();
}
