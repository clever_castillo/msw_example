import { rest } from "msw";

const handlers = [
  rest.get(
    "https://jsonplaceholder.typicode.com/users",
    async (req, res, ctx) => {
      return res(ctx.json([]));
    }
  ),
];

export default handlers;
