import React, { useState, useEffect } from "react";
import axios from "axios";

import { STATUS } from "./api";
import UserCard from "./components/UserCard";
import UserProfile from "./components/UserProfile";

import styles from "./App.module.css";

function App() {
  const [data, setData] = useState([]);
  const [status, setStatus] = useState(STATUS.IDLE);
  const [userId, setUserId] = useState();

  useEffect(() => {
    setStatus(STATUS.LOADING);
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then(response => {
        setStatus(STATUS.SUCCESS);
        console.log(response);
        setData(response.data);
      })
      .catch(_err => {
        setStatus(STATUS.ERROR);
      });
  }, []);

  if (status === STATUS.IDLE) return null;

  if (status === STATUS.LOADING) return <p>Loading...</p>;

  if (status === STATUS.ERROR) return <p>Oops... there was an error</p>;

  return (
    <div className={styles.container}>
      <div className={styles.cards}>
        {data.map(user => (
          <UserCard
            key={user.id}
            user={user}
            isSelected={userId === user.id}
            onClick={() => setUserId(user.id)}
          />
        ))}

        {data.length === 0 && <p>Ooops... there are no users</p>}
      </div>
      <div className={styles.posts}>
        {userId != null && <UserProfile userId={userId} />}
      </div>
    </div>
  );
}

export default App;
