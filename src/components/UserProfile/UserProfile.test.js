import { render, screen } from "@testing-library/react";

import UserProfile from "./UserProfile";
import { server } from "../../mocks/server";
import { rest } from "msw";

describe("<UserProfile>", () => {
  it("renders error", async () => {
    server.use(
      rest.get(
        "https://jsonplaceholder.typicode.com/users/1/posts",
        (_req, res, ctx) => res(ctx.status(404))
      )
    );

    render(<UserProfile userId={1} />);

    expect(await screen.findByText(/there was an error/i)).toBeInTheDocument();
  });

  it("renders 0 posts", async () => {
    server.use(
      rest.get(
        "https://jsonplaceholder.typicode.com/users/1/posts",
        (_req, res, ctx) => res(ctx.json([]))
      )
    );

    render(<UserProfile userId={1} />);

    expect(await screen.findByText("Posts (0)")).toBeInTheDocument();
  });
});
