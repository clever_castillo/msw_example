import React, { useState, useEffect } from "react";
import axios from "axios";

import { STATUS } from "../../api";

import styles from "./UserProfile.module.css";

function UserProfile({ userId }) {
  const [data, setData] = useState([]);
  const [status, setStatus] = useState(STATUS.IDLE);

  useEffect(() => {
    setStatus(STATUS.LOADING);

    axios
      .get(`https://jsonplaceholder.typicode.com/users/${userId}/posts`)
      .then(response => {
        setStatus(STATUS.SUCCESS);
        setData(response.data);
      })
      .catch(err => {
        setStatus(STATUS.ERROR);
      });
  }, [userId]);

  if (status === STATUS.IDLE) return null;

  if (status === STATUS.LOADING) return <p>Loading...</p>;

  if (status === STATUS.ERROR) return <p>Oops... there was an error</p>;

  return (
    <div className={styles.posts}>
      <h2>Posts ({data.length})</h2>
      {data.map(post => (
        <div key={post.id} className={styles.post}>
          <strong>{post.title}</strong>
          <p>{post.body}</p>
        </div>
      ))}
    </div>
  );
}

export default UserProfile;
