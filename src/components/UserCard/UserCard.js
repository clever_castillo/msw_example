import React from "react";

import styles from "./UserCard.module.css";

function UserCard({ user, onClick, isSelected }) {
  return (
    <div className={styles.card}>
      <h2>{user.name}</h2>

      {isSelected ? (
        <button disabled>Already selected</button>
      ) : (
        <button onClick={onClick}>Select</button>
      )}
    </div>
  );
}

export default UserCard;
